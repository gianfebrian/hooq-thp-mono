[![pipeline status](https://gitlab.com/gianfebrian/hooq-thp-mono/badges/master/pipeline.svg)](https://gitlab.com/gianfebrian/hooq-thp-mono/commits/master)
[![coverage report](https://gitlab.com/gianfebrian/hooq-thp-mono/badges/master/coverage.svg)](https://gitlab.com/gianfebrian/hooq-thp-mono/commits/master)

# HOOQ THP API #

The project uses dummy caching approach.

### API Server/Worker Configuration ###

API server configuration template can be found inside `packages/hooq-thp-api/config` folder.
```
# APPLICATION
# Aplication environment
ENV=development
# Application server port
APP_PORT=3000

# REDIS
# Redis server/connection url
REDIS_URL=redis://localhost

# TMDB API
# Important! Please replace this with your own tmdb key
TMDB_API_KEY=APIKEY
# We can skip this part and use default.
TMDB_API_VERSION=3
TMDB_API_BASE_URL=https://api.themoviedb.org

# QUEUE
# We can skip this part and use default.
QUEUE_REMOVE_ON_SUCCESS=true

# LOGGER
LOG_FORMAT=json
LOG_SILENT=false
```

### Local Run Without Docker ###
To run without docker. First:
```
npm install && cd packages/hooq-thp-api && npm install && cd - \
    cd packages/hooq-thp-client && npm install && cd -
```

Copy default configuration template:
```
cp packages/hooq-thp-api/config/default.env packages/hooq-thp-api/.env
```

Make sure redis intance instance is running. Then to run the API server:
```
npx lerna run server --scope hooq-thp-api
```

We must have at least a single queue worker:
```
npx lerna run worker --scope hooq-thp-api
```

To run react app. We need to provide our api base url by adding `REACT_APP_API_URL` to our `hooq-thp-client/.env`.

For example `REACT_APP_API_URL=http://localhost:3000/api`. Then we can start:
```
npx lerna run start --scope hooq-thp-client
```

### Run with Local Build Docker ###
Run with local build docker. First let's build it:
```
docker build -t hooq-thp .
```

Our docker build entry point is a run script with these following options:
```
scripts/run.sh [one of available options].

options:

--app 		        Run app server.
--worker 	        Run queue worker.
--coverage 	        Run unit test and collect coverage.
--integration 	        Run integration test. Make sure redis is running.
--lint 		        Run code linter.
--help 		        To see this help.
```

For example to show coverage summary:
```
docker run hooq-thp -c '--coverage'
```
Or to run the server
```
docker run hooq-thp -c '--server' -e 'TMDB_API_KEY=your_api_key' -e 'REDIS_URL=redis_instance_url'
```

### Simpler is Better ###
For simpler approach to run this project. Create a file `.env.local` containing `TMDB_API_KEY=your_api_key`. And run:
```
docker-compose up -d
```

Alternatively you can use remote image on gitlab registry.
```
docker login registry.gitlab.com -u gitlab+deploy-token-43569 -p nRFM_ED8scRyy8_hSD7h
```

*Note: deploy token will be expired on Feb 15, 2019*

Then we can use remote docker compose file
```
docker-compose -f docker-compose.remote.yml up -d
```

We can find this project docker image on gitlab registry. **[Visit Registry](https://gitlab.com/gianfebrian/hooq-thp-mono/container_registry)**

