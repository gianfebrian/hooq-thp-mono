FROM node:10.15.0 as client-builder

RUN mkdir /client

ADD packages/hooq-thp-client/package.json /client/package.json

WORKDIR /client

RUN npm install

COPY packages/hooq-thp-client .

RUN REACT_APP_ENV=production npm run build

FROM node:10.15.0-alpine

RUN mkdir /api

ADD packages/hooq-thp-api/package.json /api/package.json

WORKDIR /api

RUN npm install

COPY --from=client-builder /client/build public

COPY packages/hooq-thp-api .

RUN cp config/docker.env .env

ENTRYPOINT [ "scripts/run.sh" ]

