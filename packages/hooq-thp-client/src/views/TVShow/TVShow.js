
import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import TV from './../../api/TV';

import {
  InputGroup,
  InputGroupAddon,
  Button,
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  CardSubtitle,
  Alert,
} from 'reactstrap';

import TVShowDetail from './TVShowDetail';
import TVSeasonList from './TVSeasonList';

class TVShow extends Component {
  state = {
    tv: {},
    seasonList: [],
    season: {},
    runtime: 0,
    crews: [],
    isLoading: false,
  }

  getTVDetail = (id) => {
    this.setState({ isLoading: true });
    return TV.getDetail(id).then(res => {
      if (res.error) return null;

      this.setState({ isLoading: false });
      return this.setState({ tv: res });
    });
  }

  getTVSeason = (id, seasonNo, runtime) => {
    this.setState({ isLoading: true });
    return TV.getSeason(id, seasonNo, runtime).then(res => {
      if (res.error) return null;

      this.setState({ isLoading: false });
      return this.setState({ season: res });
    });
  }

  showLoading = () => {
    if (!this.state.isLoading) return null;

    return (
      <Alert color="info">
        <div class="d-flex align-items-center">
          <strong>Loading...</strong>
          <div class="spinner-border ml-auto" role="status" aria-hidden="true"></div>
        </div>
      </Alert>
    );
  }

  componentDidMount() {
    this.tvId = this.props.match.params.id;
    this.seasonNo = this.props.match.params.seasonNo || 1;
    this.getTVDetail(this.tvId);
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.tv !== prevState.tv) {
      this.getTVSeason(this.tvId, this.seasonNo, this.state.tv.runtime);
    }

    if (this.props.location.pathname !== prevProps.location.pathname) {
      this.tvId = this.props.match.params.id;
      this.seasonNo = this.props.match.params.seasonNo;
      this.getTVSeason(this.tvId, this.seasonNo, this.state.tv.runtime);
    }
  }

  render() {
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-md-4 summary">
            <TVShowDetail
              poster={this.state.tv.poster}
              genres={this.state.tv.genres}
              language={this.state.tv.language}
              guestStars={this.state.season.guestStars}
              overview={this.state.tv.overview} />
          </div>
          <div className="col-md-8 episodes">
            <Card>
              <CardHeader className="bg-secondary">
                <CardTitle className="text-light">Take Point</CardTitle>
                <CardSubtitle className="text-light">
                  {this.state.tv.numberOfSeasons} Seasons, {this.state.tv.numberOfEpisodes} Episodes, TVPG(US), {this.state.tv.language}
                </CardSubtitle>
                <InputGroup>
                  <InputGroupAddon addonType="prepend">
                    <Button className="btn-danger text-light">
                      <i className="fa fa-play-circle"></i>
                    </Button>
                  </InputGroupAddon>
                  <Button className="btn-danger text-left text-light">Watch First Episode</Button>
                </InputGroup>
              </CardHeader>
              <CardBody>
                <CardTitle>Episodes</CardTitle>
                <CardSubtitle>
                  Seasons: {(this.state.tv.seasonNumberList || []).map(item => (
                    <Link to={`/tv-show/${this.tvId}/season/${item}`} key={item}> {item} </Link>
                  ))}

                </CardSubtitle>
                {this.showLoading()}
                <TVSeasonList episodes={this.state.season.episodes} />
              </CardBody>
            </Card>
          </div>
        </div>
      </div>
    );
  }
}

export default TVShow;
