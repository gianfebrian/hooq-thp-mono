
import React, { Component } from 'react';

import { Button } from 'reactstrap';

class TVSeasonList extends Component {

  render() {
    return (
      <div className="table-responsive-md">
        <table className="table">
          <thead>
            <tr>
              <th style={{ width: 10 + '%' }}></th>
              <th style={{ width: 80 + '%' }}></th>
              <th style={{ width: 20 + '%' }}></th>
              <th style={{ width: 10 + '%' }}></th>
            </tr>
          </thead>
          <tbody>
            {(this.props.episodes || []).map((episode, i) => (
              <tr key={episode.no}>
                <td>{episode.no}</td>
                <td>{episode.name}</td>
                <td>{episode.runtime}</td>
                <td><Button className="text-dark"><i className="fa fa-play-circle"></i></Button></td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}

export default TVSeasonList;
