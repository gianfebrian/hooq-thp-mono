
import React, { Component } from 'react';

import {
  InputGroup,
  InputGroupAddon,
  Button,
  Card,
  CardImg,
  CardBody,
  CardTitle,
  CardSubtitle,
} from 'reactstrap';

class TVShowDetail extends Component {
  render() {
    return (
      <Card>
        <CardImg className="mx-auto d-block" src={this.props.poster} alt="image" />
        <InputGroup>
          <InputGroupAddon addonType="prepend">
            <Button className="btn-light">
              <i className="fa fa-clock-o"></i>
            </Button>
          </InputGroupAddon>
          <Button className="form-control btn-light text-left">Watch Later</Button>
        </InputGroup>
        <InputGroup>
          <InputGroupAddon addonType="prepend">
            <Button className="btn-light">
              <i className="fa fa-heart"></i>
            </Button>
          </InputGroupAddon>
          <Button className="form-control btn-light text-left">Favorite</Button>
        </InputGroup>
        <CardBody>
          <CardTitle>Genre</CardTitle>
          <CardSubtitle>{this.props.genres}</CardSubtitle>
          <CardTitle>Language</CardTitle>
          <CardSubtitle>{this.props.language}</CardSubtitle>
          <CardTitle>Casts:</CardTitle>
          <CardSubtitle>{this.props.guestStars}</CardSubtitle>
          <CardTitle>Synopsis:</CardTitle>
          <CardSubtitle>{this.props.overview}</CardSubtitle>
        </CardBody>
      </Card>
    );
  }
}

export default TVShowDetail;
