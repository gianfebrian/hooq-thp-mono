import Catalog from './Catalog';
import TVShow from './TVShow';

export { Catalog, TVShow };
