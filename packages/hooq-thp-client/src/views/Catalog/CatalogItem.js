import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {
  Card,
  CardImg,
  CardBody,
  CardTitle,
  CardSubtitle,
} from 'reactstrap';


class CatalogItem extends Component {
  render() {
    return (
      <React.Fragment>
        <Link to={`/tv-show/${this.props.id}`} className="col-auto mb-3 item">
          <Card>
            <CardImg top width="100%" src={this.props.poster} alt={this.props.name} />
            <CardBody>
              <CardTitle>TV Show</CardTitle>
              <CardSubtitle className="text-truncate">{this.props.name}</CardSubtitle>
            </CardBody>
          </Card>
        </Link>
      </React.Fragment>
    );
  }
}

export default CatalogItem;
