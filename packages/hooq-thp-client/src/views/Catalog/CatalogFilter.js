import React, { Component } from 'react';
import Select from 'react-select';
import {
  Card,
  CardHeader,
  Form,
  Row,
  Col,
  FormGroup,
  Label,
  // Input,
} from 'reactstrap';

import store from './Store';

class CatalogFilter extends Component {
  state = {
    selectedGenre: null,
    selectedLanguage: null,
    selectedSort: null,
  }

  handleChangeGenre = (selectedGenre) => {
    this.setState({ selectedGenre });
  }

  handleChangeLanguage = (selectedLanguage) => {
    this.setState({ selectedLanguage });
  }

  handleChangeSort = (selectedSort) => {
    this.setState({ selectedSort });
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.selectedGenre !== prevState.selectedGenre) {
      this.props.selectionChange('genre', this.state.selectedGenre);
    }
    if (this.state.selectedLanguage !== prevState.selectedLanguage) {
      this.props.selectionChange('language', this.state.selectedLanguage);
    }
    if (this.state.selectedSort !== prevState.selectedSort) {
      this.props.selectionChange('sort', this.state.selectedSort);
    }
  }

  render() {
    return (
      <React.Fragment>
        <Card>
          <CardHeader className="filter-card">
            <Form>
              <Row form>
                <Col md={4}>
                  <FormGroup>
                    <Label for="filter-genre">Genre</Label>
                    <Select
                      closeMenuOnSelect={false}
                      isMulti
                      value={this.state.selectedGenre}
                      onChange={this.handleChangeGenre}
                      options={store.genres} />
                  </FormGroup>
                </Col>
                <Col md={4}>
                  <FormGroup>
                    <Label for="filter-languages">Languages</Label>
                    <Select
                      value={this.state.selectedLanguage}
                      onChange={this.handleChangeLanguage}
                      options={store.languages} />
                  </FormGroup>
                </Col>
                <Col md={4}>
                  <FormGroup>
                    <Label for="sort-by">Sort By</Label>
                    <Select
                      value={this.state.selectedSort}
                      onChange={this.handleChangeSort}
                      options={store.sorts} />
                  </FormGroup>
                </Col>
              </Row>
            </Form>
          </CardHeader>
        </Card>
      </React.Fragment>
    );
  }
}

export default CatalogFilter;
