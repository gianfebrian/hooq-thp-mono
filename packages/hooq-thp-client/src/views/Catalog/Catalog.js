import React, { Component } from 'react';

import TV from './../../api/TV';
import CatalogFilter from './CatalogFilter';
import CatalogItem from './CatalogItem';

class Catalog extends Component {
  state = {
    catalogItems: [],
    height: window.innerHeight,
    nextPage: 1,
    currentPage: 1,
    toBottom: 1,
    params: {},
  }

  handleScroll = () => {
    const windowHeight = "innerHeight" in window ? window.innerHeight : document.documentElement.offsetHeight;
    const body = document.body;
    const html = document.documentElement;
    const docHeight = Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
    const windowBottom = windowHeight + window.pageYOffset;

    if (windowBottom >= docHeight - 32 && !this.state.isRefreshing) {
      return this.setState({
        toBottom: this.state.toBottom + 1,
      });
    }
  }

  getItems(params) {
    this.setState({ isRefreshing: true });

    TV.discover(params)
      .then((res) => {
        if (res.error) return null;

        return this.setState({
          catalogItems: this.state.catalogItems.concat(res),
          currentPage: this.state.currentPage,
          nextPage: this.state.nextPage + 1,
          isRefreshing: false,
        });
      });
  }

  handleFilter = (filterType, selection) => {
    const params = Object.assign({}, this.state.params);
    if (filterType === 'genre') {
      delete params.with_genres;
      if (selection.length > 0) {
        params.with_genres = selection.map(v => v.value).join(',');
      }
    }
    if (filterType === 'language') {
      delete params.with_original_language;
      if (selection !== '') {
        params.with_original_language = selection.value;
      }
    }
    if (filterType === 'sort') {
      delete params.sort_by;
      if (selection !== '') {
        params.sort_by = selection.value;
      }
    }

    this.setState({ params, catalogItems: [] });
  }

  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll);
    this.getItems({ page: 1 })
  }

  componentWillUnmount() {
    window.removeEventListener("scroll", this.handleScroll);
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.params !== prevState.params) {
      const params = this.state.params;
      params.page = this.state.currentPage;
      this.getItems(params);
    }

    if (this.state.toBottom !== prevState.toBottom) {
      const params = this.state.params;
      params.page = this.state.nextPage;
      this.getItems(params);
    }
  }

  render() {
    return (
      <div>
        <h3 className="section-heading text-light text-center">All TV Shows</h3>
        <CatalogFilter selectionChange={this.handleFilter} />
        <div className="container-fluid">
          <div className="row">
            {this.state.catalogItems.map(item => (
              <CatalogItem
                key={item.id}
                id={item.id}
                name={item.name}
                poster={item.poster}
              />
            ))}
          </div>
          <div className="row justify-content-center page-spinner">
            <div className="spinner-border" role="status">
              <span className="sr-only">Loading...</span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Catalog;
