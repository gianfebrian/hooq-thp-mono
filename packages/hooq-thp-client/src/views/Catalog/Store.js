const genres = [
  { 'label': 'Action', 'value': 28 },
  { 'label': 'Adventure', 'value': 12 },
  { 'label': 'Animation', 'value': 16 },
  { 'label': 'Comedy', 'value': 35 },
  { 'label': 'Crime', 'value': 80 },
  { 'label': 'Documentary', 'value': 99 },
  { 'label': 'Drama', 'value': 18 },
  { 'label': 'Family', 'value': 10751 },
  { 'label': 'Fantasy', 'value': 14 },
  { 'label': 'History', 'value': 36 },
  { 'label': 'Horror', 'value': 27 },
  { 'label': 'Music', 'value': 10402 },
  { 'label': 'Mystery', 'value': 9648 },
  { 'label': 'Romance', 'value': 10749 },
  { 'label': 'Science Fiction', 'value': 878 },
  { 'label': 'TV Movie', 'value': 10770 },
  { 'label': 'Thriller', 'value': 53 },
  { 'label': 'War', 'value': 10752 },
  { 'label': 'Western', 'value': 37 },
];

const sorts = [
  { label: 'Recommended', value: '' },
  { label: 'Popularity asc', value: 'popularity.asc' },
  { label: 'Popularity desc', value: 'popularity.desc' },
  { label: 'Release Date asc', value: 'release_date.asc' },
  { label: 'Release Date desc', value: 'release_date.desc' },
  { label: 'Revenue asc', value: 'revenue.asc' },
  { label: 'Revenue desc', value: 'revenue.desc' },
  { label: 'Primary Release Date asc', value: 'primary_release_date.asc' },
  { label: 'Primary Release Date desc', value: 'primary_release_date.desc' },
  { label: 'Original Title asc', value: 'original_title.asc' },
  { label: 'Original Title desc', value: 'original_title.desc' },
  { label: 'Vote Average asc', value: 'vote_average.asc' },
  { label: 'Vote Average desc', value: 'vote_average.desc' },
  { label: 'Vote Count asc', value: 'vote_count.asc' },
  { label: 'Vote Count desc', value: 'vote_count.desc' },
];

const languages = [
  { label: 'All', value: '' },
  { label: 'English', value: 'en' },
  { label: 'Indonesia', value: 'id' },
]

export default { genres, sorts, languages }
