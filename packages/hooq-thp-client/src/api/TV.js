
import axios from 'axios';
import moment from 'moment';
import ISO6391 from 'iso-639-1';

const apiURL = process.env.REACT_APP_API_URL || 'http://localhost:3000';
const tmdbImageURL = process.env.REACT_APP_TMDB_IMAGE_URL || 'https://image.tmdb.org/t/p/w185';

const maxStars = 6;
const maxCatalogItemPerRow = 6;
const maxWordsOverView = 50;

const client = axios.create();

if (process.env.REACT_APP_ENV === 'development')  {
  client.defaults.baseURL = apiURL;
}

const delay = (num) => (new Promise(resolve => setTimeout(() => (resolve()), num)));

const clientRetry = (res) => {
  if (typeof res.config.counter === 'undefined') res.config.counter = 1;

  if (res.data.error !== null && res.config.counter < 5) {
    res.config.counter++;
    return delay(res.config.counter * 1000).then(() => (client(res.config)));
  }

  return res;
};

client.interceptors.response.use(clientRetry);

const buildTMDBImageURL = (imgPath = '') => {
  const imgName = (imgPath || '').replace('/', '');

  return `${tmdbImageURL}/${imgName}`;
};

const compactCatalogGrid = (items = [], maxPerRow) => {
  const reminder = items.length % maxPerRow;

  return items.slice(0, -reminder);
};

const discover = async (params) => {
  const res = await client.get('/api/discover', { params });
  const { data, error } = res.data;
  if (error !== null) {
    return { error };
  }

  const catalogItems = compactCatalogGrid(data.results, maxCatalogItemPerRow);
  return catalogItems.map(item => ({
    id: item.id,
    name: item.name,
    poster: buildTMDBImageURL(item.poster_path),
  }));
}

const getSeasonNumberList = (numberOfSeasons = 0) => {
  return [...Array(numberOfSeasons + 1).keys()].slice(1);
};

const getRuntime = (runtimes = [0]) => {
  const runtime = runtimes.slice(0, 1).shift();

  return moment.utc(moment.duration(runtime, 'minutes').as('milliseconds')).format('HH:mm:ss');
};

const getGenreString = (genres = [{}]) => {
  return genres.map(g => g.name).join(', ');
};

const getShortOverview = (overview = '', maxWords) => {
  return `${overview.split(' ').slice(0, maxWords).join(' ')}...`;
};

const getDetail = async (tvId) => {
  const res = await client.get(`/api/tv/${tvId}`);
  const { data, error } = res.data;
  if (typeof error !== undefined && error !== null) {
    return { error };
  }

  return {
    id: data.id,
    poster: buildTMDBImageURL(data.poster_path),
    genres: getGenreString(data.genres),
    overview: getShortOverview(data.overview, maxWordsOverView),
    numberOfSeasons: data.number_of_seasons,
    numberOfEpisodes: data.number_of_episodes,
    parentalGuide: '-',
    language: ISO6391.getName(data.original_language),
    seasonNumberList: getSeasonNumberList(data.number_of_seasons),
    runtime: getRuntime(data.episode_run_time),
  };
}

const getGuestStars = (episodes = [{ guest_stars: [] }], maxStars) => {
  return episodes[0].guest_stars.map(star => (star.name)).slice(0, maxStars).join(', ');
};

const getSeason = async (tvId, seasonNo = 1, episodeRuntime) => {
  const res = await client.get(`/api/tv/${tvId}/season/${seasonNo}`);
  const { data, error } = res.data;
  if (typeof error !== undefined && error !== null) {
    return { error };
  }

  const episodes = data.episodes.map((ep, i) => ({
    id: ep.id,
    no: i + 1,
    name: ep.name,
    runtime: episodeRuntime,
  }));

  return {
    episodes,
    id: data.id,
    guestStars: getGuestStars(data.episodes, maxStars),
  };
};

export default {
  buildTMDBImageURL,
  compactCatalogGrid,
  discover,
  getSeasonNumberList,
  getRuntime,
  getGenreString,
  getShortOverview,
  getDetail,
  getGuestStars,
  getSeason,
}
