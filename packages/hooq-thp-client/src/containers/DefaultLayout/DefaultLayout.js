import React, { Component, Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Container, Row, Col } from 'reactstrap';

// routes config
import routes from '../../routes';
import DefaultNavbar from './DefaultNavbar';

class DefaultLayout extends Component {

  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

  signOut(e) {
    e.preventDefault()
    this.props.history.push('/login')
  }

  render() {
    return (
      <div className="app">
        <DefaultNavbar />
        <div className="nav-content-separator bg-secondary"></div>
        <Container className="content">
          <Row>
            <Col sm="12" md={{ size: 10, offset: 1 }}>
              <Suspense fallback={this.loading()}>
                <Switch>
                  {routes.map((route, idx) => {
                    return route.component ? (
                      <Route
                        key={idx}
                        path={route.path}
                        exact={route.exact}
                        name={route.name}
                        render={props => (
                          <route.component {...props} />
                        )} />
                    ) : (null);
                  })}
                  <Redirect from="/" to="/catalog" />
                </Switch>
              </Suspense>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default DefaultLayout;
