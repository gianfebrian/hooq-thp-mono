import React from 'react';
import DefaultLayout from './containers/DefaultLayout';

const Catalog = React.lazy(() => import('./views/Catalog'));
const TVShow = React.lazy(() => import('./views/TVShow'));


// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: '/', exact: true, name: 'Home', component: DefaultLayout },
  { path: '/catalog', name: 'Catalog', component: Catalog },
  { path: '/tv-show/:id/season/:seasonNo', name: 'TVShow', component: TVShow },
  { path: '/tv-show/:id', name: 'TVShow', component: TVShow },

];

export default routes;
