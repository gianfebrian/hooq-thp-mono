/* global describe, test, expect, beforeEach, beforeAll, afterAll */

require('dotenv').config();

const url = require('url');
const nock = require('nock');
const Redis = require('ioredis');
const request = require('supertest');

const server = require('./../src/app');
const TMDB = require('./../src/tmdb');
const Storage = require('./../src/storage');

const apiKey = process.env.TMDB_API_KEY;
const baseURL = process.env.TMDB_API_BASE_URL;
const version = process.env.TMDB_API_VERSION;
const tmdb = new TMDB(apiKey, baseURL, version);

const redis = new Redis(process.env.REDIS_URL);
const storage = new Storage();

let interceptor;

const flushRedis = async () => {
  await redis.flushall();
};

const queueComparator = (eventName, jobId, resolve) => (
  (obj) => {
    if (eventName === 'stalled' && obj === jobId) {
      return resolve();
    }

    if (obj.id === jobId) {
      return resolve();
    }

    return null;
  }
);

const waitForQueueEvent = async (eventName, jobId) => {
  let cmp;
  const listener = () => (
    new Promise((resolve) => {
      cmp = queueComparator(eventName, jobId, resolve);
      server.worker.queue.on(eventName, cmp);
    })
  );

  await listener();
  server.worker.queue.removeListener(eventName, cmp);
};

beforeEach(async () => {
  nock.cleanAll();
  interceptor = nock(baseURL);
  await flushRedis();
});

beforeAll(async () => {
  server.worker.start();
});

afterAll(async () => {
  await flushRedis();
});

describe('Discover TV Exhibitions', () => {
  test('Should return discover empty data for the first request', async () => {
    const queries = [['page', 1]];
    const apiURL = url.parse(tmdb.buildURL(TMDB.getDiscoverTVParams(), queries));

    interceptor.get(apiURL.path).reply(200, {});

    const res = await request(server.app)
      .get(`/api/discover?${TMDB.buildQuery(queries)}`)
      .expect(200);

    expect(res.body.data).toEqual(null);

    await waitForQueueEvent('succeeded', storage.buildCacheQuery(queries), null);
  });

  test('Should return discover data for the second request', async () => {
    const queries = [['page', 1]];
    const apiURL = url.parse(tmdb.buildURL(TMDB.getDiscoverTVParams(), queries));

    interceptor.get(apiURL.path).reply(200, { fake: 'data' });

    await request(server.app)
      .get(`/api/discover?${TMDB.buildQuery(queries)}`)
      .expect(200);

    await waitForQueueEvent('succeeded', storage.buildCacheQuery(queries), null);

    const res = await request(server.app)
      .get(`/api/discover?${TMDB.buildQuery(queries)}`)
      .expect(200);

    expect(res.body.data).toEqual({ fake: 'data' });
  });

  test.skip('Should re-cache after 1 week', () => { });
});

describe('TV Show Exhibitions', () => {
  test('Should return TV empty data for the first request', async () => {
    const apiURL = url.parse(tmdb.buildURL(TMDB.getTVParams(1)));

    interceptor.get(apiURL.path).reply(200, {});

    const res = await request(server.app)
      .get(`/api/${TMDB.buildParams(TMDB.getTVParams(1))}`)
      .expect(200);

    expect(res.body.data).toEqual(null);

    await waitForQueueEvent('succeeded', TMDB.getTVParams(1).join('_'), null);
  });

  test('Should return TV data for the second request', async () => {
    const apiURL = url.parse(tmdb.buildURL(TMDB.getTVParams(1)));

    interceptor.get(apiURL.path).reply(200, { fake: 'data' });

    await request(server.app)
      .get(`/api/${TMDB.buildParams(TMDB.getTVParams(1))}`)
      .expect(200);

    await waitForQueueEvent('succeeded', TMDB.getTVParams(1).join('_'), null);

    const res = await request(server.app)
      .get(`/api/${TMDB.buildParams(TMDB.getTVParams(1))}`)
      .expect(200);

    expect(res.body.data).toEqual({ fake: 'data' });
  });

  test.skip('Should re-cache season after next episode to air date', () => { });
});

describe('TV Season Exhibitions', () => {
  test('Should return TV Season empty data for the first request', async () => {
    const apiURL = url.parse(tmdb.buildURL(TMDB.getTVSeasonParams(1, 2)));

    interceptor.get(apiURL.path).reply(200, {});

    const res = await request(server.app)
      .get(`/api/${TMDB.buildParams(TMDB.getTVSeasonParams(1, 2))}`)
      .expect(200);

    expect(res.body.data).toEqual(null);

    await waitForQueueEvent('succeeded', TMDB.getTVSeasonParams(1, 2).join('_'), null);
  });

  test('Should return TV Season data for the second request', async () => {
    const apiURL = url.parse(tmdb.buildURL(TMDB.getTVSeasonParams(1, 2)));

    interceptor.get(apiURL.path).reply(200, { fake: 'data' });

    await request(server.app)
      .get(`/api/${TMDB.buildParams(TMDB.getTVSeasonParams(1, 2))}`)
      .expect(200);

    await waitForQueueEvent('succeeded', TMDB.getTVSeasonParams(1, 2).join('_'), null);

    const res = await request(server.app)
      .get(`/api/${TMDB.buildParams(TMDB.getTVSeasonParams(1, 2))}`)
      .expect(200);

    expect(res.body.data).toEqual({ fake: 'data' });
  });
});
