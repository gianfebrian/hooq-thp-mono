
const _ = require('lodash');

class Storage {
  constructor(redis) {
    this.redis = redis;

    this.termSymbol = '_';
    this.keyTermSymbol = ':';
    this.parentKey = 'cache';
  }

  buildCacheKey(...keys) {
    const chain = [this.parentKey, ...keys];
    _.remove(chain, k => _.isEqual(k, []) || _.isUndefined(k) || _.isNull(k) || _.trim(k, ' ') === '');

    return _.join(chain, this.keyTermSymbol);
  }

  buildCacheQuery(queries) {
    const queryObject = _.fromPairs(queries);
    const genres = _(queryObject.with_genres)
      .split(',')
      .map(_.toNumber)
      .compact()
      .sortBy()
      .value()
      .join('_');

    const chain = [genres, queryObject.translation, queryObject.sort_by, queryObject.page];
    _.remove(chain, k => _.isEqual(k, []) || _.isUndefined(k) || _.isNull(k) || _.trim(k, ' ') === '');

    return _.join(chain, this.termSymbol);
  }

  saveCache(params, queries, data) {
    const keys = [...params];

    if (_.includes(params, 'discover')) {
      keys.push(this.buildCacheQuery(queries));
    }

    const builtKey = this.buildCacheKey(...keys);

    return this.redis.set(builtKey, data);
  }

  findCache(params, queries) {
    const keys = [...params];

    if (_.includes(params, 'discover')) {
      keys.push(this.buildCacheQuery(queries));
    }

    const builtKey = this.buildCacheKey(...keys);

    return this.redis.get(builtKey);
  }
}

module.exports = Storage;
