/* global test, expect */

const nock = require('nock');

const TMDB = require('./tmdb');

const apiKey = 'fakeapikey';
const baseURL = 'https://api.themoviedb.org';
const version = '3';

test('getDiscoverTVParams()', () => {
  const res = TMDB.getDiscoverTVParams();

  expect(res).toEqual(['discover', 'tv']);
});

test('getTVParams()', () => {
  const res = TMDB.getTVParams(1);

  expect(res).toEqual(['tv', 1]);
});

test('getTVSeasonParams()', () => {
  const res = TMDB.getTVSeasonParams(1, 1);

  expect(res).toEqual(['tv', 1, 'season', 1]);
});

test('buildParams()', () => {
  const cases = [
    {
      params: undefined,
      expected: '',
      error: false,
    },
    {
      params: ['discover', 'tv'],
      expected: 'discover/tv',
      error: false,
    },
    {
      params: [],
      expected: '',
      error: false,
    },
  ];

  cases.forEach((c) => {
    const url = TMDB.buildParams(c.params);

    expect(url).toBe(c.expected);
  });
});

test('buildQuery()', () => {
  const cases = [
    {
      queries: undefined,
      expected: '',
      error: false,
    },
    {
      queries: [],
      expected: '',
      error: false,
    },
    {
      queries: [['1', '2', '3']],
      expected: '',
      error: false,
    },
    {
      queries: [['sort_by', 'popularity_desc']],
      expected: 'sort_by=popularity_desc',
      error: false,
    },
  ];

  cases.forEach((c) => {
    const url = TMDB.buildQuery(c.queries);

    expect(url).toBe(c.expected);
  });
});

test('buildURL()', () => {
  const cases = [
    {
      params: undefined,
      queries: undefined,
      expected: 'https://api.themoviedb.org/3?api_key=fakeapikey',
      error: false,
    },
    {
      params: ['discovery', 'tv'],
      queries: undefined,
      expected: 'https://api.themoviedb.org/3/discovery/tv?api_key=fakeapikey',
      error: false,
    },
    {
      params: ['discovery', 'tv'],
      queries: [['sort_by', 'popularity_desc']],
      expected: 'https://api.themoviedb.org/3/discovery/tv?api_key=fakeapikey&sort_by=popularity_desc',
      error: false,
    },
  ];

  cases.forEach((c) => {
    const tmdb = new TMDB(apiKey, baseURL, version);
    const url = tmdb.buildURL(c.params, c.queries);

    expect(url).toBe(c.expected);
  });
});

test('makeRequest()', () => {
  const cases = [
    {
      params: ['discover', 'tv'],
      nock: {
        paramQuery: `/${version}/discover/tv?api_key=${apiKey}`,
        response: {
          statusCode: 200,
          body: {},
        },
      },
      expected: {},
      error: false,
    },
  ];

  cases.forEach(async (c) => {
    nock(`${baseURL}`)
      .get(c.nock.paramQuery)
      .reply(c.nock.response.statusCode, c.nock.response.body);

    const tmdb = new TMDB(apiKey, baseURL, version);
    const res = await tmdb.makeRequest(c.params, c.queries);

    expect(JSON.parse(res)).toEqual(c.expected);
  });
});
