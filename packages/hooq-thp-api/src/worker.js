
const { logger } = require('./logger');
const q = require('./queue');

class Worker {
  constructor(queue, storage, tmdb) {
    this.queue = queue;
    this.storage = storage;
    this.tmdb = tmdb;
  }

  start() {
    this.queue.on('ready', () => {
      logger.info('Queue processor is ready');
      this.queue.process(q.processor(this.storage, this.tmdb));
    });

    this.queue.on('error', (err) => {
      logger.error(`A queue error happened: ${err.message}`);
    });

    this.queue.on('succeeded', (job, result) => {
      logger.info(`Job ${job.id} succeeded with result: ${result}`);
    });

    this.queue.on('retrying', (job, err) => {
      logger.info(`Job ${job.id} failed with error ${err.message} but is being retried!`);
    });

    this.queue.on('failed', (job, err) => {
      logger.info(`Job ${job.id} failed with error ${err.message}`);
    });

    this.queue.on('stalled', (jobId) => {
      logger.info(`Job ${jobId} stalled and will be reprocessed`);
    });
  }
}

module.exports = Worker;
