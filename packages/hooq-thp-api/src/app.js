
const _ = require('lodash');
const Redis = require('ioredis');
const express = require('express');
const Queue = require('bee-queue');
const morgan = require('morgan');

const { logger, overrideStream } = require('./logger');
const TMDB = require('./tmdb');
const Storage = require('./storage');
const Worker = require('./worker');
const api = require('./api');

const apiKey = process.env.TMDB_API_KEY;
const baseURL = process.env.TMDB_API_BASE_URL;
const version = process.env.TMDB_API_VERSION;

const tmdb = new TMDB(apiKey, baseURL, version);
const redis = new Redis(process.env.REDIS_URL);
const storage = new Storage(redis);

const queue = new Queue('tmdb-request', {
  redis: {
    url: process.env.REDIS_URL,
  },
  activateDelayedJobs: true,
  removeOnSuccess: _.toUpper(process.env.QUEUE_REMOVE_ON_SUCCESS) === 'TRUE',
});

const worker = new Worker(queue, storage, tmdb);

const router = express.Router();
router.get('/discover', api.getDiscover(queue, storage, TMDB));
router.get('/tv/:id', api.getTV(queue, storage, TMDB));
router.get('/tv/:id/season/:no', api.getTVSeason(queue, storage, TMDB));

const app = express();
overrideStream();
app.use((req, res, next) => {
  if (_.toUpper(process.env.ENV) !== 'PRODUCTION') {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  }

  next();
});
app.use(morgan('tiny', { stream: logger.stream }));
app.use('/api', router);
app.use(express.static('public'));

module.exports = { app, worker };
