/* global test */

const queue = require('./queue');

const mockTmdb = {
  makeRequest: function makeRequest(params, queries) {
    return Promise.resolve([params, queries]);
  },
};

const mockStorage = {
  saveCache: function saveCache() {
    return Promise.resolve('OK');
  },
};

test('processor()', async () => {
  const job = {
    data: {
      params: null,
      queries: null,
    },
  };

  await queue.processor(mockStorage, mockTmdb)(job);
});
