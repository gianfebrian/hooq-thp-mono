/* global test, expect */

const Redis = require('ioredis-mock');

const Storage = require('./storage');

test('buildCacheKey()', () => {
  const cases = [
    {
      title: 'Undefined parameter should only return parent key',
      test: {
        keys: [undefined],
      },
      expected: 'cache',
      error: false,
    },
    {
      title: 'Undefined/Null/(Empty array/string) should be included in key chain',
      test: {
        keys: [undefined, null, [], '', 0],
      },
      expected: 'cache:0',
      error: false,
    },
    {
      title: 'Should make a chain for valid keys',
      test: {
        keys: ['a', 'b'],
      },
      expected: 'cache:a:b',
      error: false,
    },
  ];

  cases.forEach((c) => {
    const storage = new Storage(null);

    const res = storage.buildCacheKey(...c.test.keys);

    expect(res).toBe(c.expected);
  });
});

test('buildCacheQuery()', () => {
  const cases = [
    {
      title: 'Page null should not be included in query chain',
      test: {
        queries: [
          ['with_genres', '20,3, 4, , null'],
          ['sort_by', 'popularity_desc'],
          ['page', null],
        ],
      },
      expected: '3_4_20_popularity_desc',
      error: false,
    },
    {
      title: 'Other undefined should not be included in query chain',
      test: {
        queries: [
          ['page', 1],
        ],
      },
      expected: '1',
      error: false,
    },
    {
      title: 'Should make a chain for valid queries',
      test: {
        queries: [
          ['with_genres', '20,3, 4, , null'],
          ['translation', 'english'],
          ['sort_by', 'popularity_desc'],
          ['page', 1],
        ],
      },
      expected: '3_4_20_english_popularity_desc_1',
      error: false,
    },
  ];

  cases.forEach((c) => {
    const storage = new Storage(null);

    const res = storage.buildCacheQuery(c.test.queries);

    expect(res).toBe(c.expected);
  });
});

test('saveCache()', async () => {
  const cases = [
    {
      title: 'Discover should success',
      test: {
        params: ['discover', 'tv'],
        queries: [
          ['with_genres', '20,3, 4, , null'],
          ['translation', 'english'],
          ['sort_by', 'popularity_desc'],
          ['page', 1],
        ],
        data: {
          fake: 'data',
        },
      },
      expected: 'OK',
      error: false,
    },
    {
      title: 'Non discover should success',
      test: {
        params: ['tv', 1],
        data: {
          fake: 'data',
        },
      },
      expected: 'OK',
      error: false,
    },
  ];

  cases.forEach(async (c) => {
    const storage = new Storage(new Redis());
    const res = await storage.saveCache(c.test.params, c.test.queries, c.test.data);

    expect(res).toBe(c.expected);
  });
});

test('findCache()', async () => {
  const cases = [
    {
      title: 'Discover should success',
      test: {
        params: ['discover', 'tv'],
        queries: [
          ['with_genres', '20,3, 4, , null'],
          ['translation', 'english'],
          ['sort_by', 'popularity_desc'],
          ['page', 1],
        ],
        data: {
          'cache:discover:tv:3_4_20_english_popularity_desc_1': {
            fake: 'data',
          },
        },
      },
      expected: {
        fake: 'data',
      },
      error: false,
    },
    {
      title: 'Non discover should success',
      test: {
        params: ['tv', '1'],
        data: {
          'cache:tv:1': {
            fake: 'data',
          },
        },
      },
      expected: {
        fake: 'data',
      },
      error: false,
    },
  ];

  cases.forEach(async (c) => {
    const redis = new Redis({
      data: c.test.data,
    });
    const storage = new Storage(redis);
    const res = await storage.findCache(c.test.params, c.test.queries);

    expect(res).toEqual(c.expected);
  });
});
