
const processor = (storage, tmdb) => (
  async (job) => {
    const { params, queries } = job.data;

    const res = await tmdb.makeRequest(params, queries);

    return storage.saveCache(params, queries, res);
  }
);

module.exports = { processor };
