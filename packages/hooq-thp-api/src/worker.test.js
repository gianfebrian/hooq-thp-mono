/* global test */

const EventEmitter = require('events');

const Worker = require('./worker');

const mockTmdb = {
  makeRequest: function makeRequest(params, queries) {
    return Promise.resolve([params, queries]);
  },
};

const mockStorage = {
  saveCache: function saveCache() {
    return Promise.resolve('OK');
  },
};

class MockQueue extends EventEmitter {
  process(processor) {
    processor({ data: {} });

    return this;
  }
}
const mockQueue = new MockQueue();

test('start()', () => {
  const worker = new Worker(mockQueue, mockStorage, mockTmdb);
  worker.start();
  mockQueue.emit('ready');
  mockQueue.emit('succeeded', { id: 1 }, 'success');
  mockQueue.emit('error', Error('error'));
  mockQueue.emit('retrying', { id: 1 }, Error('error'));
  mockQueue.emit('failed', { id: 1 }, Error('error'));
  mockQueue.emit('stalled', 1);
});
