
const _ = require('lodash');
const moment = require('moment');

const getDiscover = (queue, storage, TMDB) => (
  async (req, res) => {
    const params = TMDB.getDiscoverTVParams();
    const queries = _.toPairs(req.query);

    const resp = await storage.findCache(params, queries);
    if (_.isEmpty(resp)) {
      await queue.createJob({ params, queries })
        .setId(storage.buildCacheQuery(queries))
        .save();

      return res.json({
        error: 'waiting in a line',
        data: null,
      });
    }

    return res.json({
      error: null,
      data: JSON.parse(resp),
    });
  }
);

const cacheNextEpisodeToAir = (queue, TMDB, tvId, resp, todayDate) => {
  const tv = JSON.parse(resp);
  if (!_.isEmpty(tv.next_episode_to_air)) {
    const when = tv.next_episode_to_air.air_date;
    const momentWhen = moment(when);

    if (_.isEmpty(when) || !momentWhen.isValid()) {
      return null;
    }

    const today = moment(todayDate || {});
    const diff = Math.abs(momentWhen.diff(today, 'days'));
    // only recache if next episode will be aired within 7 days
    if (diff > 7) {
      return null;
    }

    const params = TMDB.getTVSeasonParams(tvId, tv.next_episode_to_air.season_number);
    const queries = [];

    return queue.createJob({ params, queries })
      .setId(_.join([_.snakeCase(when), ...params], '_'))
      .delayUntil(momentWhen.toDate())
      .save();
  }

  return null;
};

const getTV = (queue, storage, TMDB) => (
  async (req, res) => {
    const params = TMDB.getTVParams(req.params.id);
    const queries = [];

    const resp = await storage.findCache(params, queries);
    if (_.isEmpty(resp)) {
      await queue.createJob({ params, queries })
        .setId(_.join(params, '_'))
        .save();

      return res.json({
        error: 'waiting in a line',
        data: null,
      });
    }

    // this is indeed will re-touch the queue each time we fetch tv
    // but the next episode delay job will only be saved once,
    // any retouch to the queue won't recreate the same delay job.
    await cacheNextEpisodeToAir(queue, TMDB, req.params.id, resp);

    return res.json({
      error: null,
      data: JSON.parse(resp),
    });
  }
);

const getTVSeason = (queue, storage, TMDB) => (
  async (req, res) => {
    const params = TMDB.getTVSeasonParams(req.params.id, req.params.no);
    const queries = [];

    const resp = await storage.findCache(params, queries);
    if (_.isEmpty(resp)) {
      await queue.createJob({ params, queries })
        .setId(_.join(params, '_'))
        .save();

      return res.json({
        error: 'waiting in a line',
        data: null,
      });
    }

    return res.json({
      error: null,
      data: JSON.parse(resp),
    });
  }
);

module.exports = {
  getDiscover,
  getTV,
  getTVSeason,
  cacheNextEpisodeToAir,
};
