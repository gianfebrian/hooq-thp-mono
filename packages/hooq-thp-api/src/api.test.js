/* global test, expect */

const EventEmitter = require('events');
const moment = require('moment');
const Redis = require('ioredis-mock');
const TMDB = require('./tmdb');
const Storage = require('./storage');
const api = require('./api');

const makeRequest = obj => obj;
const res = {
  json(obj) { return obj; },
};

class Queue extends EventEmitter {
  setId(id) {
    this.id = id;

    return this;
  }

  createJob(data) {
    this.data = data;

    return this;
  }

  delayUntil(date) {
    this.date = date;

    return this;
  }

  save() {
    return new Promise(resolve => (resolve({
      id: this.id,
      data: this.data,
      date: this.date,
    })));
  }
}

test('getDiscover()', async () => {
  const cases = [
    {
      title: 'Should return error "waiting in a line" when no data in cache',
      test: {
        data: {},
        query: {},
        params: {},
      },
      expected: { data: null, error: 'waiting in a line' },
      error: false,
    },
    {
      title: 'Should return error "waiting in a line" when no data in cache',
      test: {
        data: { 'cache:discover:tv': JSON.stringify({ fake: 'data' }) },
        query: {},
        params: {},
      },
      expected: { data: { fake: 'data' }, error: null },
      error: false,
    },
  ];

  cases.forEach(async (c) => {
    const redis = new Redis({ data: c.test.data });
    const queue = new Queue();

    const storage = new Storage(redis);

    const req = makeRequest({ query: c.test.query, params: c.test.params });
    const resp = await api.getDiscover(queue, storage, TMDB)(req, res);

    expect(resp).toEqual(c.expected);
  });
});

test('getTV()', async () => {
  const cases = [
    {
      title: 'Should return error "waiting in a line" when no data in cache',
      test: {
        data: {},
        query: {},
        params: { id: 1 },
      },
      expected: { data: null, error: 'waiting in a line' },
      error: false,
    },
    {
      title: 'Should return error "waiting in a line" when no data in cache',
      test: {
        data: { 'cache:tv:1': JSON.stringify({ fake: 'data' }) },
        query: {},
        params: { id: 1 },
      },
      expected: { data: { fake: 'data' }, error: null },
      error: false,
    },
  ];

  cases.forEach(async (c) => {
    const redis = new Redis({ data: c.test.data });
    const queue = new Queue();

    const storage = new Storage(redis);

    const req = makeRequest({ query: c.test.query, params: c.test.params });
    const resp = await api.getTV(queue, storage, TMDB)(req, res);

    expect(resp).toEqual(c.expected);
  });
});

test('getTVSeason()', async () => {
  const cases = [
    {
      title: 'Should return error "waiting in a line" when no data in cache',
      test: {
        data: {},
        query: {},
        params: { id: 1, no: 2 },
      },
      expected: { data: null, error: 'waiting in a line' },
      error: false,
    },
    {
      title: 'Should return error "waiting in a line" when no data in cache',
      test: {
        data: { 'cache:tv:1:season:2': JSON.stringify({ fake: 'data' }) },
        query: {},
        params: { id: 1, no: 2 },
      },
      expected: { data: { fake: 'data' }, error: null },
      error: false,
    },
  ];

  cases.forEach(async (c) => {
    const redis = new Redis({ data: c.test.data });
    const queue = new Queue();

    const storage = new Storage(redis);

    const req = makeRequest({ query: c.test.query, params: c.test.params });
    const resp = await api.getTVSeason(queue, storage, TMDB)(req, res);

    expect(resp).toEqual(c.expected);
  });
});

test('cacheNextEpisodeToAir()', async () => {
  const cases = [
    {
      title: 'Should return null since the next episode object is empty',
      test: {
        data: {},
        query: {},
        params: { id: 1, no: 2 },
        resp: {
          next_episode_to_air: null,
        },
      },
      expected: null,
      error: false,
    },
    {
      title: 'Should return null since the next episode object is empty',
      test: {
        data: {},
        query: {},
        params: { id: 1, no: 2 },
        resp: {
          next_episode_to_air: {},
        },
      },
      expected: null,
      error: false,
    },
    {
      title: 'Should return null since the next episode date is invalid',
      test: {
        data: {},
        query: {},
        params: { id: 1, no: 2 },
        resp: {
          next_episode_to_air: {
            air_date: {},
          },
        },
      },
      expected: null,
      error: false,
    },
    {
      title: 'Should return null since the next episode date is invalid',
      test: {
        data: {},
        query: {},
        params: { id: 1, no: 2 },
        resp: {
          next_episode_to_air: {
            air_date: '',
          },
        },
      },
      expected: null,
    },
    {
      title: 'Should return null since the next episode date will be aired in more than 7 days',
      test: {
        data: {},
        query: {},
        params: { id: 1, no: 2 },
        resp: {
          next_episode_to_air: {
            air_date: '2019-02-11',
          },
        },
        todayDate: moment('2019-02-11').add(8, 'days'),
      },
      expected: null,
    },
    {
      title: 'Should return jobs object',
      test: {
        data: {},
        query: {},
        params: { id: 1, no: 2 },
        resp: {
          next_episode_to_air: {
            air_date: '2019-02-11',
            season_number: 2,
          },
        },
      },
      expected: {
        id: '2019_02_11_tv_1_season_2',
        data: {
          params: ['tv', 1, 'season', 2],
          queries: [],
        },
        date: moment('2019-02-11').toDate(),
      },
      error: false,
    },
  ];

  cases.forEach(async (c) => {
    const queue = new Queue();

    const req = makeRequest({ query: c.test.query, params: c.test.params });
    const tvId = req.params.id;
    const resp = JSON.stringify(c.test.resp);
    const cache = await api.cacheNextEpisodeToAir(queue, TMDB, tvId, resp, c.test.todayDate);

    expect(cache).toEqual(c.expected);
  });
});
