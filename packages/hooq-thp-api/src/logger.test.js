/* global test, expect, jest */

// eslint-disable-next-line
const { logger, formatter, isSilent, overrideStream } = require('./logger');

test('formatter()', () => {
  expect.assertions(3);
  const json = formatter('json');
  expect(json.constructor.name).toBe('Format');
  const simple = formatter('simple');
  expect(simple.constructor.name).toBe('Format');
  const defaultFormat = formatter();
  expect(defaultFormat.constructor.name).toBe('Format');
});

test('isSilent()', () => {
  expect.assertions(4);
  const trueSilent = isSilent('true');
  expect(trueSilent).toBe(true);
  const falseSilent = isSilent('false');
  expect(falseSilent).toBe(false);
  const emptySilent = isSilent();
  expect(emptySilent).toBe(true);
  const otherSilent = isSilent('xxxx');
  expect(otherSilent).toBe(false);
});

test('overrideStream()', () => {
  expect.assertions(2);
  overrideStream();
  const writer = jest.fn(logger.stream.write);
  expect(logger.stream).toHaveProperty('write');
  writer('test');
  expect(writer).toHaveBeenCalled();
});
