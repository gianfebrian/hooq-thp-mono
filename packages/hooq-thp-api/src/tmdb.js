
const _ = require('lodash');
const request = require('request-promise-native');

class TMDB {
  constructor(apiKey, baseURL, version) {
    this.apiKey = apiKey;
    this.baseURL = baseURL;
    this.version = version;
  }

  static getDiscoverTVParams() {
    return ['discover', 'tv'];
  }

  static getTVParams(id) {
    return ['tv', id];
  }

  static getTVSeasonParams(tvId, seasonNo) {
    return ['tv', tvId, 'season', seasonNo];
  }

  static buildParams(params) {
    if (_.isUndefined(params) || _.isEmpty(params)) {
      return '';
    }

    return params.join('/');
  }

  static buildQuery(queries) {
    if (_.isUndefined(queries) || _.isEmpty(queries)) {
      return '';
    }

    const pairs = [];
    queries.forEach((v) => {
      if (_.isUndefined(v) || _.isEmpty(v) || v.length > 2) {
        return null;
      }

      return pairs.push(v.join('='));
    });

    return pairs.join('&');
  }

  buildURL(params, queries) {
    const urlChains = [this.baseURL, this.version];
    if (!_.isUndefined(params) && !_.isEmpty(params)) {
      urlChains.push(...params);
    }
    const url = this.constructor.buildParams(urlChains);

    const queryChains = [['api_key', this.apiKey]];
    if (!_.isUndefined(queries) && !_.isEmpty(queries)) {
      queryChains.push(...queries);
    }
    const query = this.constructor.buildQuery(queryChains);

    return `${url}?${query}`;
  }

  makeRequest(params, queries) {
    const url = this.buildURL(params, queries);

    return request.get(url);
  }
}

module.exports = TMDB;
