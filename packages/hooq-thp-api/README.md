# HOOQ THP API #

The project uses dummy caching approach.

To run without docker:
```
npm install
```

Then:
```
scripts/run.sh [one of available options].

options:

--app 		        Run app server.
--worker 	        Run queue worker.
--coverage 	        Run unit test and collect coverage.
--integration 	        Run integration test. Make sure redis is running.
--lint 		        Run code linter.
--help 		        To see this help.
```

Run with docker with local build:
```
docker build -t hooq-thp-api .
```

Then we can pass above run script options when running the image, for example to show coverage summary:
```
docker run hooq-thp-api -c '--coverage'
```

This project also publish docker image to GitLab container repo.

