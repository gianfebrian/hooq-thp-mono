#!/usr/bin/env sh

show_help() {
  printf "\nscripts/run.sh [one of available options]."
  printf "\n\noptions:"
  printf "\n\n--app \t\t Run app server."
  printf "\n--worker \t Run queue worker."
  printf "\n--coverage \t Run unit test and collect coverage."
  printf "\n--integration \t Run integration test. Make sure redis is running."
  printf "\n--lint \t\t Run code linter."
  printf "\n--help \t\t To see this help.\n\n"

  exit 0
}

while test $# -gt 0
do
  case "$1" in
    --app)
      FLAG=app;src/bin/www;
      ;;
    --worker)
      FLAG=worker;src/bin/worker;
      ;;
    --coverage)
      FLAG=coverage;npm test;
      ;;
    --integration)
      FLAG=integration;npm run integration;
      ;;
    --lint)
      FLAG=lint;npm run lint;
      ;;
    --help)
      FLAG=help;show_help;
      ;;
  esac
  shift
done

if [ -z $FLAG ]; then
  echo ""
  echo "Instance type is required. See --help for available options."
  echo ""
  exit 1
fi
